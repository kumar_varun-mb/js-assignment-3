
function flatten(elements) {
    let output=[];

    function insideFlatten(elements){           // created a helper function so that with each recursion value of output array is not initialized, and previous value lost.
        for(let i=0; i<elements.length;i++){
            if(Array.isArray(elements[i]))
                insideFlatten(elements[i]);
            else
                output.push(elements[i]);
        }
    }

    insideFlatten(elements);        //calling the helper function
    return output;
}
module.exports = flatten;