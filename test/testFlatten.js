let flatten=require('../flatten.js');

const nestedArray = [1, [2], [[3]], [[[4]]]]; //sample input

console.log(flatten(nestedArray));