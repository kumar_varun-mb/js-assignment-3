function filter(elements, cb) {
    let output=[];
    for(let i=0;i<elements.length;i++){
        if(cb(elements[i], i, elements))     // if true is returned by callback function
            output.push(elements[i]);
    }
    return output;
}
module.exports = filter;