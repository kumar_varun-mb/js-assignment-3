function find(elements, cb) {
    for(let i=0;i<elements.length;i++){
        if(cb(elements[i], i, elements))     //if true is returned by callback function for the 1st occurence
            return elements[i];
    }
    return undefined;   //if no match found, undefined is returned.
}
module.exports = find;