function map(elements, cb) {
    let output=[];
    for(let i=0;i<elements.length;i++){
        output.push(cb(elements[i], i, elements));       // pushes value to output array which is returned by custom call back function
    }
    return output;
}
module.exports=map;