function reduce(elements, cb, startingValue) {
    let i;
    if(!startingValue){             // if no starting value, then element[0] is starting value and i starts from next index
        startingValue=elements[0];
        i=1;
    }
    for(i=0;i<elements.length;i++){
        startingValue=cb(startingValue,elements[i], i, elements);
    }
    return startingValue;
 }
module.exports = reduce;